import 'package:flutter/material.dart';
import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaNombrar extends StatelessWidget {
  const PantallaNombrar(this.totalJugadores, this.listaJugadores, {Key? key})
      : super(key: key);
  final int totalJugadores;
  final List<String> listaJugadores;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(child: Text('Nombrar jugadores')),
          backgroundColor: Colors.black54,
        ),
        body: CuerpoNombrarJugadores(totalJugadores, listaJugadores),
        bottomNavigationBar: OptInf());
  }
}

class CuerpoNombrarJugadores extends StatefulWidget {
  const CuerpoNombrarJugadores(this.totalJugadores, this.listaJugadores,
      {Key? key})
      : super(key: key);
  final int totalJugadores;
  final List<String> listaJugadores;

  @override
  State<CuerpoNombrarJugadores> createState() =>
      _CuerpoNombrarJugadoresState(totalJugadores, listaJugadores);
}

class _CuerpoNombrarJugadoresState extends State<CuerpoNombrarJugadores> {
  int totalJugadores;
  List<String> listaJugadores;
  _CuerpoNombrarJugadoresState(this.totalJugadores, this.listaJugadores);

  List<String> nombreJugadores = [];

  @override
  Widget build(BuildContext context) {
    nombreJugadores = listaJugadores;

    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Elige un nombre para tus jugadores',
                style: estiloMensaje)),
        Icon(
          Icons.edit,
          size: 100.0,
          color: Colors.grey,
        ),
        Expanded(
            child: ListView.builder(
          itemCount: nombreJugadores.length,
          itemBuilder: (context, index) {
            return Column(
              children: [
                Divider(
                  height: 20.0,
                ),
                TextField(
                  textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0)),
                      hintText: 'Jugador ${index + 1}',
                      labelText: 'Jugador ${index + 1}',
                      helperText: 'Escriba el nombre de jugador',
                      suffixIcon: Icon(Icons.accessibility),
                      icon: Icon(Icons.arrow_right_alt)),
                  onChanged: (valor) {
                    setState(() {
                      nombreJugadores[index] = valor;

                      context.read<BlocUno>().add(EditandoNombrarJugadores(
                          nombresEditados: nombreJugadores));
                    });
                  },
                ),
              ],
            );
          },
        )),
      ],
    );
  }
}

class OptInf extends StatelessWidget {
  const OptInf({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Colors.red.shade300,
                  onPrimary: Colors.white,
                  shadowColor: Colors.teal,
                  elevation: 5,
                  textStyle: const TextStyle(
                    fontSize: 12,
                    fontStyle: FontStyle.italic,
                  )),
              onPressed: () => _mostrarAdvertencia(context),
              onLongPress: () {
                context.read<BlocUno>().add(SalirDePartida());
              },
              child: Text('Ir a inicio'),
            ),
          ),
          SizedBox(
            width: 190.0,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    )),
                onPressed: () {
                  _mostrarAdvertencia2(context);
                },
                onLongPress: () {
                  context.read<BlocUno>().add(ValidaNombresJugadores());
                },
                child: Text('Siguiente')),
          )
        ],
      ),
    );
  }

  void _mostrarAdvertencia(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            title: Text('Advertencia'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(salirDePartida),
                iconoAdvertencia,
              ],
            ),
            actions: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    )),
                onPressed: () => Navigator.of(context).pop(),
                child: Text('Aceptar'),
              ),
            ],
          );
        });
  }

  void _mostrarAdvertencia2(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            title: Center(child: Text('Mensaje')),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Center(child: Text(mensajeNombresJug)),
                iconoAdvertencia,
              ],
            ),
            actions: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    )),
                onPressed: () => Navigator.of(context).pop(),
                child: Text('Aceptar'),
              ),
            ],
          );
        });
  }
}
