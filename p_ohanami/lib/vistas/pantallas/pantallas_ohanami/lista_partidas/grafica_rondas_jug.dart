import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:p_ohanami/app/constantes.dart';
import 'package:p_ohanami/bloc/eventos.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:p_ohanami/bloc/bloc.dart';
import 'package:p_ohanami/repositorio/clases_data/rondas.dart';

class PantallaGraficaRondas extends StatelessWidget {
  const PantallaGraficaRondas(this.lista, this.indice, this.index, {Key? key})
      : super(key: key);
  final List<dynamic> lista;
  final int indice;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Estadística Rondas')),
        backgroundColor: Colors.black54,
        actions: [RegresarADetallesPartida(indice)],
      ),
      body: CuerpoGrafica(lista, indice, index),
    );
  }
}

class RegresarADetallesPartida extends StatelessWidget {
  const RegresarADetallesPartida(this.indice, {Key? key}) : super(key: key);
  final int indice;
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: ChoiceChip(
        label: Text('Regresar'),
        selected: false,
        onSelected: (select) {
          //context.read<BlocUno>().add(CerrandoSesion());
          context
              .read<BlocUno>()
              .add(VerDetallesDePartida(partidaSeleccionada: indice));
        },
      ),
    ));
  }
}

class CuerpoGrafica extends StatefulWidget {
  const CuerpoGrafica(this.lista, this.indice, this.index, {Key? key})
      : super(key: key);

  final List<dynamic> lista;
  final int indice;
  final int index;

  @override
  _CuerpoGraficaState createState() =>
      _CuerpoGraficaState(lista, indice, index);
}

class _CuerpoGraficaState extends State<CuerpoGrafica> {
  List<dynamic> lista;
  int indice;
  int index;

  _CuerpoGraficaState(this.lista, this.indice, this.index);

  List<charts.Series<Rondas, String>> _seriesData = [];

  _generateData() {
    var azules = [
      Rondas(r1, lista[indice][jgs][index][r1][azl]),
      Rondas(r2, lista[indice][jgs][index][r2][azl]),
      Rondas(r3, lista[indice][jgs][index][r3][azl]),
    ];

    var verdes = [
      Rondas(r1, lista[indice][jgs][index][r1][vrd]),
      Rondas(r2, lista[indice][jgs][index][r2][vrd]),
      Rondas(r3, lista[indice][jgs][index][r3][vrd]),
    ];

    var negras = [
      Rondas(r1, lista[indice][jgs][index][r1][ngr]),
      Rondas(r2, lista[indice][jgs][index][r2][ngr]),
      Rondas(r3, lista[indice][jgs][index][r3][ngr]),
    ];

    var rosas = [
      Rondas(r1, lista[indice][jgs][index][r1][rss]),
      Rondas(r2, lista[indice][jgs][index][r2][rss]),
      Rondas(r3, lista[indice][jgs][index][r3][rss]),
    ];

    _seriesData.add(
      charts.Series(
        domainFn: (Rondas rondas, _) => rondas.ronda,
        measureFn: (Rondas rondas, _) => rondas.valorCarta,
        id: 'azules',
        data: azules,
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Rondas rondas, _) =>
            charts.ColorUtil.fromDartColor(Color(0xff01579b)),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Rondas rondas, _) => rondas.ronda,
        measureFn: (Rondas rondas, _) => rondas.valorCarta,
        id: 'verdes',
        data: verdes,
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Rondas rondas, _) =>
            charts.ColorUtil.fromDartColor(Color(0xff109618)),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Rondas rondas, _) => rondas.ronda,
        measureFn: (Rondas rondas, _) => rondas.valorCarta,
        id: 'negras',
        data: negras,
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Rondas rondas, _) =>
            charts.ColorUtil.fromDartColor(Color(0xFF616161)),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Rondas rondas, _) => rondas.ronda,
        measureFn: (Rondas rondas, _) => rondas.valorCarta,
        id: 'rosas',
        data: rosas,
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Rondas pollution, _) =>
            charts.ColorUtil.fromDartColor(Color(0xffec407a)),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _generateData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(
      child: Column(
        children: [
          Text('Cantidad de Cartas por Ronda',
              style: TextStyle(fontWeight: FontWeight.bold)),
          Expanded(
              child: charts.BarChart(
            _seriesData,
            animate: true,
            barGroupingType: charts.BarGroupingType.grouped,
            animationDuration: Duration(seconds: 3),
          ))
        ],
      ),
    ));
  }
}
