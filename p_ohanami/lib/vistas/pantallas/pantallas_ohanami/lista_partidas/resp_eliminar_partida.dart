import 'package:flutter/material.dart';
import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class RespEliminarPartida extends StatelessWidget {
  const RespEliminarPartida(this.mensaje,{ Key? key }) : super(key: key);
  final String mensaje;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Eliminar Partida')),
        backgroundColor: Colors.black54,
      ),
      body: Eliminada(mensaje),
    );
  }
}

class Eliminada extends StatelessWidget {
  const Eliminada(this.mensaje,{ Key? key }) : super(key: key);
  final String mensaje;
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
          children: [
            SizedBox(height: 30),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Text(mensaje,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        color: const Color(0xFF616161))),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Icon(Icons.error_outline, size: 70, color: Colors.red.shade300),
            SizedBox(
              height: 100.0,
            ),
            ElevatedButton(
              style:  ElevatedButton.styleFrom(
                primary: Colors.red.shade300,
                onPrimary: Colors.white,
                shadowColor: Colors.teal,
                elevation: 5,
                textStyle: const TextStyle(
                  fontSize: 20,
                  fontStyle: FontStyle.italic,
                )),

              onPressed: () {
                 context.read<BlocUno>().add(AbrirListaPartidas());
              },
              child: Text('Regresar a partidas'),
            )
          ],
        ),
      );
  }
}