import 'package:flutter/material.dart';

import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaRegistro extends StatelessWidget {
  const PantallaRegistro({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Center(child: Text('Registrar Usuario')),
          backgroundColor: Colors.black54),
      body: const CuerpoRegistro(),
    );
  }
}

class CuerpoRegistro extends StatelessWidget {
  const CuerpoRegistro({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Contenido();
  }
}

class Contenido extends StatefulWidget {
  const Contenido({Key? key}) : super(key: key);

  @override
  _ContenidoState createState() => _ContenidoState();
}

class _ContenidoState extends State<Contenido> {
   String nombreRegistro = "";
   String contrasena = "";

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      children: [
        Center(
          child: Text('Nuevo Registro',
              style: const TextStyle(
                  //fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                  color: const Color(0xFF616161))),
        ),
        Divider(
          height: 30.0,
        ),
        TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
              hintText: 'Nombre',
              labelText: 'Nombre',
              helperText: 'Ingrese un nombre de usuario para registrar',
              suffixIcon: Icon(Icons.accessibility),
              icon: Icon(Icons.account_circle)),
          onChanged: (valor) {
            setState(() {
              nombreRegistro = valor;
            });
          },
        ),
        Divider(
          height: 30.0,
        ),
        TextField(
            obscureText: true,
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0)),
                hintText: 'Contraseña',
                labelText: 'Contraseña',
                helperText: 'Ingrese una contraseña',
                suffixIcon: Icon(Icons.lock_open),
                icon: Icon(Icons.lock)),
            onChanged: (valor) => setState(() {
                contrasena = valor;

                })),
        SizedBox(height: 40),
        Center(
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.red.shade300,
                onPrimary: Colors.white,
                shadowColor: Colors.teal,
                elevation: 5,
                textStyle: const TextStyle(
                  fontSize: 20,
                  fontStyle: FontStyle.italic,
                )),
            onPressed: () {
              context
                  .read<BlocUno>()
                  .add(CargandoNuevoRegistro(nombre: nombreRegistro, contrasena: contrasena));
            },
            child: const Text('Registrar'),
          ),
        ),
        SizedBox(height: 20),
        Center(
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.black54,
                onPrimary: Colors.white,
                shadowColor: Colors.teal,
                elevation: 5,
                textStyle: const TextStyle(
                  fontSize: 20,
                  fontStyle: FontStyle.italic,
                )),
            onPressed: () {
              context.read<BlocUno>().add(AbrirLogin());
            },
            child: const Text('Regresar a login'),
          ),
        ),
      ],
    );
  }
}
