import 'package:flutter/material.dart';
import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaCargandoNuevoRegistro extends StatelessWidget {
  const PantallaCargandoNuevoRegistro(this.nombreUsuario, this.contrasena,{Key? key}) : super(key: key);
  final String nombreUsuario;
  final String contrasena;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Center(child: Text('Nuevo Registro')),
          backgroundColor: Colors.black54),
      body: Cargando(nombreUsuario, contrasena),
    );
  }
}

class Cargando extends StatelessWidget {
  const Cargando(this.nombreUsuario, this.contrasena,{Key? key}) : super(key: key);
  final String nombreUsuario;
  final String contrasena;

  @override
  Widget build(BuildContext context) {
    
    context.read<BlocUno>().add(Registrar(nombre: nombreUsuario, contrasena: contrasena));

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Container(
                width: 200, height: 200, child: CircularProgressIndicator(
                  color: Colors.red.shade300,
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Center(
              child: Text(
                msj1Registro,
                style: estiloMensaje
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: ListTile(
              title: Center(
                  child: Text(msj2Registro, style: estiloMensaje)),
              subtitle: Center(child: Text(msj3Registro)),
            ),
          )
        ],
      ),
    );
  }
}
