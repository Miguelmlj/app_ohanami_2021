import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//estilo Fuente
const estiloMensaje = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 15.0,
    color: const Color(0xFF616161));

const iconoMensaje = Icon(Icons.error_outline, size: 70, color: Colors.grey);

const iconoAdvertencia = Icon(
  Icons.warning,
  size: 100.0,
  color: Colors.grey,
);

const iconoGame = Icon(Icons.stacked_bar_chart, size: 70, color: Colors.grey);

const estiloBarPartida = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 15.0,
    color: Colors.white);


const estiloJugPart = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 20.0,
    color: Colors.black54);


const estiloJugPunt = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 20.0,
    color: Colors.teal); 


const estiloJugGan = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 18.0,
    color: Colors.green);            
