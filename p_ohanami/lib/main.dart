import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:p_ohanami/bloc/bloc.dart';
import 'package:p_ohanami/repositorio/rep_bd/rep_mongo.dart';
import 'package:p_ohanami/vistas/aplicacion.dart';

void main() {
  runApp(const Proveedor());
}

class Proveedor extends StatelessWidget {
  const Proveedor({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => RepMongo(),
      child: BlocProvider(
          create: (context) => BlocUno(context.read<RepMongo>()),
          child: const Aplicacion()),
    );
  }
}
