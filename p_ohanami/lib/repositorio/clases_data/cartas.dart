import 'dart:convert';

class Cartas {
  int azules;
  int verdes;
  int negras;
  int rosas;
  Cartas({
    required this.azules,
    required this.verdes,
    required this.negras,
    required this.rosas,
  });
  

  Cartas copyWith({
    int? azules,
    int? verdes,
    int? negras,
    int? rosas,
  }) {
    return Cartas(
      azules: azules ?? this.azules,
      verdes: verdes ?? this.verdes,
      negras: negras ?? this.negras,
      rosas: rosas ?? this.rosas,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'azules': azules,
      'verdes': verdes,
      'negras': negras,
      'rosas': rosas,
    };
  }

  factory Cartas.fromMap(Map<String, dynamic> map) {
    return Cartas(
      azules: map['azules']?.toInt() ?? 0,
      verdes: map['verdes']?.toInt() ?? 0,
      negras: map['negras']?.toInt() ?? 0,
      rosas: map['rosas']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory Cartas.fromJson(String source) => Cartas.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Cartas(azules: $azules, verdes: $verdes, negras: $negras, rosas: $rosas)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is Cartas &&
      other.azules == azules &&
      other.verdes == verdes &&
      other.negras == negras &&
      other.rosas == rosas;
  }

  @override
  int get hashCode {
    return azules.hashCode ^
      verdes.hashCode ^
      negras.hashCode ^
      rosas.hashCode;
  }
}
