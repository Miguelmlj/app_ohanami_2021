import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:p_ohanami/app/constantes.dart';
import 'package:p_ohanami/bloc/estados.dart';
import 'package:p_ohanami/bloc/eventos.dart';
import 'package:p_ohanami/repositorio/rep_bd/rep.dart';
import 'package:partida/partida.dart';

class BlocUno extends Bloc<Eventos, Estado> {
  
  String nombreUsuario = "";
  Rep repositorio;
  int numeroDeJugadores = 2;

  late int cartasArepartir;
  late int jugadorSeleccionadoR1;
  late int numeroDeCartasApuntuar = 4;
  late int ronda;

  List<String> nombreJugadores = [];
  List<int> numeroCartasJugador = [];
  List<Jugador> listaConJugadores = [];
  List<String> listaConJugadoresPuntuados = [];

  late Jugador j1, j2, j3, j4;
  late PRonda3 cj1, cj2, cj3, cj4;

  List<PRonda3> ronda1 = [];
  bool llave = false;
  Map<String, dynamic> partidasData = {};

  BlocUno(this.repositorio) : super(Login()) {
    //Eventos login y registro
    on<AbrirLogin>(_onAbrirLogin);
    on<AbrirRegistro>(_onAbrirRegistro);
    on<IniciarSesion>(_onIniciarSesion);
    on<CargandoIniciandoSesion>(_onCargandoIniciandoSesion);
    on<CerrandoSesion>(_onCerrandoSesion);
    on<Registrar>(_onPaginaRegistro);
    on<CargandoNuevoRegistro>(_onCargandoNuevoRegistro);

    //Eventos Ohanami
    on<AbrirNuevaPartida>(_onAbrirNuevaPartida);
    on<AbrirListaPartidas>(_onAbrirListaPartidas);
    on<SalirDePartida>(_onSalirDePartida);
    on<AbrirNombrarJugadores>(_onAbrirNombrarJugadores);
    on<CantidadDeJugadores>(_onCantidadDeJugadores);
    on<EditandoNombrarJugadores>(_onEditandoNombrarJugadores);
    on<AbrirRondaPrimera>(_onPaginaRondaPrimera);
    on<ValidaNombresJugadores>(_onValidaNombresJugadores);
    on<JugadorSeleccionadoR1>(_onJugadorSeleccionadoR1);
    on<EditandoNumeroCartasr1>(_onEditandoNumeroCartasr1);
    on<RegresarAJugadoresAPuntuar>(_onRegresarAJugadoresAPuntuar);
    on<SiguienteRonda>(_onSiguienteRonda);
    on<ResultadosObtenidosFinPart>(_onResultadosObtenidosFinPart);
    on<VerDetallesDePartida>(_onVerDetallesDePartida);
    on<EliminarPartida>(_onEliminarPartida);
    on<IntentarEliminarPartida>(_onIntentarEliminarPartida);
    on<VerEstadisticaJugador>(_onVerEstadisticaJugador);

    add(AbrirLogin());
  }

  void _onAbrirLogin(AbrirLogin evento, Emitter<Estado> emit) {
    emit(Login());
  }

  void _onAbrirRegistro(AbrirRegistro evento, Emitter<Estado> emit) {
    emit(PaginaRegistro());
  }

  FutureOr<void> _onCargandoIniciandoSesion(
      CargandoIniciandoSesion event, Emitter<Estado> emit) {
    emit(Iniciandome(event.nombre, event.contrasena));
  }

  void _onIniciarSesion(IniciarSesion evento, Emitter<Estado> emit) async {
    nombreUsuario = evento.nombre;

    final String respuestaLog = await repositorio.registradoUsuario(
        nombre: evento.nombre, contrasena: evento.contrasena);

    if (respuestaLog == mensajeAccesoLogin1) {
      final String msjdata =
          await repositorio.descargarData(nombre: evento.nombre);

      emit(PaginaPrincipal());
    } else {
      emit(PaginaErrorAcceso(respuestaLog));
    }
  }

  FutureOr<void> _onCerrandoSesion(CerrandoSesion event, Emitter<Estado> emit) {
    nombreUsuario = "";
    numeroDeJugadores = 2;
    nombreJugadores = [];
    numeroCartasJugador = [];
    listaConJugadoresPuntuados = [];
    ronda1 = [];
    repositorio.cerrarSesion();
    emit(Login());
  }

  FutureOr<void> _onCargandoNuevoRegistro(
      CargandoNuevoRegistro event, Emitter<Estado> emit) {
    emit(Registrandome(event.nombre, event.contrasena));
  }

  FutureOr<void> _onPaginaRegistro(
      Registrar evento, Emitter<Estado> emit) async {
    final String registroUsuario = await repositorio.registrarUsuario(
        nombre: evento.nombre, contrasena: evento.contrasena);

    emit(PaginaRespRegistro(registroUsuario));
  }

  FutureOr<void> _onAbrirNuevaPartida(
      AbrirNuevaPartida event, Emitter<Estado> emit) {
    emit(PaginaNuevaPartida());
  }

  FutureOr<void> _onCantidadDeJugadores(
      CantidadDeJugadores event, Emitter<Estado> emit) {
    numeroDeJugadores = event.cantidadJugadores + 2;
    emit(PaginaNuevaPartida());
  }

  FutureOr<void> _onAbrirListaPartidas(
      AbrirListaPartidas event, Emitter<Estado> emit) {
    partidasData = repositorio.obtenerPartidas();

    final List<dynamic> partidasUsuario = partidasData['partidas'];

    emit(PaginaMisPartidas(partidasUsuario));
  }

  FutureOr<void> _onVerDetallesDePartida(
      VerDetallesDePartida event, Emitter<Estado> emit) {
    int indice = event.partidaSeleccionada;

    final List<dynamic> partidaSeleccionada = partidasData['partidas'];

    emit(VerDetallesPartida(partidaSeleccionada, indice));
  }

  FutureOr<void> _onSalirDePartida(SalirDePartida event, Emitter<Estado> emit) {
    numeroDeJugadores = 2;
    nombreJugadores = [];
    numeroCartasJugador = [];
    listaConJugadoresPuntuados = [];
    ronda1 = [];
    repositorio.salirDePartida();

    emit(PaginaPrincipal());
  }

  FutureOr<void> _onAbrirNombrarJugadores(
      AbrirNombrarJugadores event, Emitter<Estado> emit) {
    nombreJugadores = [];

    if (nombreJugadores.isEmpty) {
      for (var i = 0; i < numeroDeJugadores; i++) {
        nombreJugadores.add('');
      }
    }

    emit(PaginaNombrarJugadores(numeroDeJugadores, nombreJugadores));
  }

  FutureOr<void> _onEditandoNombrarJugadores(
      EditandoNombrarJugadores event, Emitter<Estado> emit) {
    nombreJugadores = event.nombresEditados;
    emit(PaginaNombrarJugadores(numeroDeJugadores, nombreJugadores));
  }

  FutureOr<void> _onValidaNombresJugadores(
      ValidaNombresJugadores event, Emitter<Estado> emit) {
    final String mensajeValidacion =
        repositorio.registrarJugadoresPartida(nombres: nombreJugadores);

    if (mensajeValidacion == camposVacios) {
      emit(ErrorNombresJugadores(mensajeValidacion));
    } else if (mensajeValidacion == nombresRepetidos) {
      emit(ErrorNombresJugadores(mensajeValidacion));
    } else if (mensajeValidacion == nombresCorrectos) {
      ronda = 1;

      emit(PaginaRondaPrimera(nombreJugadores, ronda));
    }
  }

  FutureOr<void> _onPaginaRondaPrimera(
      AbrirRondaPrimera event, Emitter<Estado> emit) {
    emit(PaginaRondaPrimera(nombreJugadores, ronda));
  }

  FutureOr<void> _onJugadorSeleccionadoR1(
      JugadorSeleccionadoR1 event, Emitter<Estado> emit) {
    jugadorSeleccionadoR1 = event.jugadorSeleccionado;

    var existePuntuacion = listaConJugadoresPuntuados
        .contains(nombreJugadores[jugadorSeleccionadoR1]);

    if (!existePuntuacion) {
      listaConJugadoresPuntuados.add(nombreJugadores[jugadorSeleccionadoR1]);

      cartasArepartir = 10;

      if (numeroCartasJugador.isEmpty) {
        for (var i = 0; i < numeroDeCartasApuntuar; i++) {
          numeroCartasJugador.add(0);
        }
      }

      emit(PaginaCartasJugadorR1(numeroCartasJugador,
          nombreJugadores[jugadorSeleccionadoR1], cartasArepartir, ronda));
    } else {
      emit(ErrorJugadorSeleccionado(jugadorYaPuntuado));
    }
  }

  FutureOr<void> _onEditandoNumeroCartasr1(
      EditandoNumeroCartasr1 event, Emitter<Estado> emit) {
    numeroCartasJugador = event.cantidadDeCartas;
    cartasArepartir = event.totalCartasRepartidas;

    if (jugadorSeleccionadoR1 == 0) {
      llave = true;

      j1 = Jugador(nombre: nombreJugadores[jugadorSeleccionadoR1]);
      cj1 = PRonda3(
        jugador: j1,
        cuantasAzules: numeroCartasJugador[azules],
        cuantasVerdes: numeroCartasJugador[verdes],
        cuantasNegras: numeroCartasJugador[negras],
        cuantasRosas: numeroCartasJugador[rosas],
      );
    } else if (jugadorSeleccionadoR1 == 1) {
      llave = true;

      j2 = Jugador(nombre: nombreJugadores[jugadorSeleccionadoR1]);
      cj2 = PRonda3(
        jugador: j2,
        cuantasAzules: numeroCartasJugador[azules],
        cuantasVerdes: numeroCartasJugador[verdes],
        cuantasNegras: numeroCartasJugador[negras],
        cuantasRosas: numeroCartasJugador[rosas],
      );
    } else if (jugadorSeleccionadoR1 == 2) {
      llave = true;

      j3 = Jugador(nombre: nombreJugadores[jugadorSeleccionadoR1]);
      cj3 = PRonda3(
        jugador: j3,
        cuantasAzules: numeroCartasJugador[azules],
        cuantasVerdes: numeroCartasJugador[verdes],
        cuantasNegras: numeroCartasJugador[negras],
        cuantasRosas: numeroCartasJugador[rosas],
      );
    } else if (jugadorSeleccionadoR1 == 3) {
      llave = true;

      j4 = Jugador(nombre: nombreJugadores[jugadorSeleccionadoR1]);
      cj4 = PRonda3(
        jugador: j4,
        cuantasAzules: numeroCartasJugador[azules],
        cuantasVerdes: numeroCartasJugador[verdes],
        cuantasNegras: numeroCartasJugador[negras],
        cuantasRosas: numeroCartasJugador[rosas],
      );
    }

    emit(PaginaCartasJugadorR1(numeroCartasJugador,
        nombreJugadores[jugadorSeleccionadoR1], cartasArepartir, ronda));
  }

  FutureOr<void> _onRegresarAJugadoresAPuntuar(
      RegresarAJugadoresAPuntuar event, Emitter<Estado> emit) {
    if (!event.puntuado) {
      if (jugadorSeleccionadoR1 == 0) {
        if (!llave) {
          j1 = Jugador(nombre: nombreJugadores[jugadorSeleccionadoR1]);
          cj1 = PRonda3(
            jugador: j1,
            cuantasAzules: numeroCartasJugador[azules],
            cuantasVerdes: numeroCartasJugador[verdes],
            cuantasNegras: numeroCartasJugador[negras],
            cuantasRosas: numeroCartasJugador[rosas],
          );
        }

        llave = false;

        ronda1.add(cj1);
      } else if (jugadorSeleccionadoR1 == 1) {
        if (!llave) {
          j2 = Jugador(nombre: nombreJugadores[jugadorSeleccionadoR1]);
          cj2 = PRonda3(
            jugador: j2,
            cuantasAzules: numeroCartasJugador[azules],
            cuantasVerdes: numeroCartasJugador[verdes],
            cuantasNegras: numeroCartasJugador[negras],
            cuantasRosas: numeroCartasJugador[rosas],
          );
        }

        llave = false;

        ronda1.add(cj2);
      } else if (jugadorSeleccionadoR1 == 2) {
        if (!llave) {
          j3 = Jugador(nombre: nombreJugadores[jugadorSeleccionadoR1]);
          cj3 = PRonda3(
            jugador: j3,
            cuantasAzules: numeroCartasJugador[azules],
            cuantasVerdes: numeroCartasJugador[verdes],
            cuantasNegras: numeroCartasJugador[negras],
            cuantasRosas: numeroCartasJugador[rosas],
          );
        }

        llave = false;

        ronda1.add(cj3);
      } else if (jugadorSeleccionadoR1 == 3) {
        if (!llave) {
          j4 = Jugador(nombre: nombreJugadores[jugadorSeleccionadoR1]);
          cj4 = PRonda3(
            jugador: j4,
            cuantasAzules: numeroCartasJugador[azules],
            cuantasVerdes: numeroCartasJugador[verdes],
            cuantasNegras: numeroCartasJugador[negras],
            cuantasRosas: numeroCartasJugador[rosas],
          );
        }

        llave = false;

        ronda1.add(cj4);
      }
    }

    numeroCartasJugador = [];

    emit(PaginaRondaPrimera(nombreJugadores, ronda));
  }

  FutureOr<void> _onSiguienteRonda(
      SiguienteRonda event, Emitter<Estado> emit) async {
    if (listaConJugadoresPuntuados.length != numeroDeJugadores) {
      emit(ErrorJugadorSeleccionado(jugadorSinPuntuar));
    } else {
      listaConJugadoresPuntuados = [];
      numeroCartasJugador = [];

      if (ronda == 1) {
        repositorio.registrarPuntuacionesR1(jugadoresConCartas: ronda1);
        ronda1 = [];
        ronda++;

        emit(PaginaRondaPrimera(nombreJugadores, ronda));
      } else if (ronda == 2) {
        repositorio.registrarPuntuacionesR2(jugadoresConCartas: ronda1);
        ronda1 = [];
        ronda++;

        emit(PaginaRondaPrimera(nombreJugadores, ronda));
      } else if (ronda == 3) {
        repositorio.registrarPuntuacionesR3(jugadoresConCartas: ronda1);
        ronda = 0;
        ronda1 = [];

        String a = await repositorio.registrarPuntuacionFinal();

        emit(CargandoPartidaTerminada());
      }
    }
  }

  FutureOr<void> _onResultadosObtenidosFinPart(
      ResultadosObtenidosFinPart event, Emitter<Estado> emit) async {
    print(nombreUsuario);
    String response = await repositorio.subirPartida(usuario: nombreUsuario);

    if (response == partidaErrorConexion) {
      emit(ErrorConexionPartidaT(response, partidaPerdida));
    } else {
      final String msjdata =
          await repositorio.descargarData(nombre: nombreUsuario);

      if (msjdata == descargaErrorConexion) {
        //emit el mismo estado que en response
        emit(ErrorConexionPartidaT(msjdata, partidaPerdida));
      } else {
        partidasData = repositorio.obtenerPartidas();

        final List<dynamic> partidasActualizadas = partidasData['partidas'];

        emit(PartidaTerminada(partidasActualizadas));
      }
    }
  }

  FutureOr<void> _onVerEstadisticaJugador(
      VerEstadisticaJugador event, Emitter<Estado> emit) {
    emit(EstadisticaJugadorMostrada(event.lista, event.indice, event.index));
  }

  FutureOr<void> _onEliminarPartida(
      EliminarPartida event, Emitter<Estado> emit) {
    emit(EliminandoPartida(event.partidaSeleccionada));
  }

  FutureOr<void> _onIntentarEliminarPartida(
      IntentarEliminarPartida event, Emitter<Estado> emit) async {
    String eliminar = await repositorio.eliminarPartida(
        indice: event.partidaSeleccionada, usuario: nombreUsuario);

    emit(PartidaEliminada(eliminar));
  }
}
