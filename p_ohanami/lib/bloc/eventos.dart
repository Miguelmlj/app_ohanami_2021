class Eventos {}

//LOGIN==================================================
// logica rutas
class AbrirLogin extends Eventos {}

class AbrirRegistro extends Eventos {}

class IniciarSesion extends Eventos {
  final String nombre;
  final String contrasena;

  IniciarSesion({required this.nombre, required this.contrasena});
}

class CerrandoSesion extends Eventos {}

class CargandoIniciandoSesion extends Eventos {
  final String nombre;
  final String contrasena;

  CargandoIniciandoSesion({required this.nombre, required this.contrasena});
}

class CargandoNuevoRegistro extends Eventos {
  final String nombre;
  final String contrasena;

  CargandoNuevoRegistro({required this.nombre, required this.contrasena});
}

class Registrar extends Eventos {
  final String nombre;
  final String contrasena;

  Registrar({required this.nombre, required this.contrasena});
}
//LOGIN==================================================

//OHANAMI================================================
class AbrirNuevaPartida extends Eventos {}

class SalirDePartida extends Eventos {}

class CantidadDeJugadores extends Eventos {
  final int cantidadJugadores;

  CantidadDeJugadores({required this.cantidadJugadores});
}

class AbrirNombrarJugadores extends Eventos {}

class EditandoNombrarJugadores extends Eventos {
  final List<String> nombresEditados;

  EditandoNombrarJugadores({required this.nombresEditados});
}

//este evento es solo para pruebas
class RutaFicticia extends Eventos {}

class ValidaNombresJugadores extends Eventos {}

//RONDA 1
class AbrirRondaPrimera extends Eventos {}

class JugadorSeleccionadoR1 extends Eventos {
  final int jugadorSeleccionado;

  JugadorSeleccionadoR1({required this.jugadorSeleccionado});
}

class JugadorYaSeleccionado extends Eventos {}

class RegresarAJugadoresAPuntuar extends Eventos {
  final bool puntuado;

  RegresarAJugadoresAPuntuar({required this.puntuado});
}

class PuntuacionRondaPrimera extends Eventos {}

class EditandoNumeroCartasr1 extends Eventos {
  final List<int> cantidadDeCartas;
  final int totalCartasRepartidas;

  EditandoNumeroCartasr1(
      {required this.cantidadDeCartas, required this.totalCartasRepartidas});
}

class SiguienteRonda extends Eventos {}

class ResultadosObtenidosFinPart extends Eventos {}

class NoHasPuntuadoJugadores extends Eventos {}

//OHANAMI================================================
//ver partidas del jugador

class AbrirListaPartidas extends Eventos {}

class VerDetallesDePartida extends Eventos {
  final int partidaSeleccionada;

  VerDetallesDePartida({required this.partidaSeleccionada});
}

class EliminarPartida extends Eventos {
  final int partidaSeleccionada;
  

  EliminarPartida({required this.partidaSeleccionada});
}

class IntentarEliminarPartida extends Eventos {
  final int partidaSeleccionada;
  

  IntentarEliminarPartida({required this.partidaSeleccionada});
}

class VerEstadisticaJugador extends Eventos {
  final List<dynamic> lista;
  final int indice;
  final int index;

  VerEstadisticaJugador(
      {required this.lista, required this.indice, required this.index});
}
