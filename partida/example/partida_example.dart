import 'package:partida/partida.dart';

void main() {

 

  //********************example Profe******************************** */
    Jugador j1 = Jugador(nombre: 'Pepe');
    Jugador j2 = Jugador(nombre: 'Juan');
  Partida p = Partida(jugadores: {j1,j2})
  ..puntuacionRonda1(
    [PRonda1(jugador: j1, cuantasAzules: 3), PRonda1(jugador: j2, cuantasAzules: 1)])
  ..puntuacionRonda2([
    PRonda2(jugador: j1, cuantasAzules: 5, cuantasVerdes: 3),
    PRonda2(jugador: j2, cuantasAzules: 6, cuantasVerdes: 2)
  ]);

  // Partida p = Partida(jugadores: {j1,j2});
  // p.puntuacionRonda1([PRonda1(jugador: j1, cuantasAzules: 3), PRonda1(jugador: j2, cuantasAzules: 1)]);
  print(p);
 

  //********************example Profe******************************** */


}
