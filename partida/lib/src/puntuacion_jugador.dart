import 'package:partida/partida.dart';

 class P {}


class PuntuacionJugador extends P{
  final Jugador jugador;
  final int porAzules;
  final int porVerdes;
  final int porRosas;
  final int porNegras;

  int get total => porAzules + porVerdes + porNegras + porRosas;

  PuntuacionJugador({required this.jugador, required this.porAzules, required this.porVerdes, required this.porRosas, required this.porNegras});
  
}


class PuntuacionJugadorFinal extends P{
  final Jugador jugador;
  final int total;

  PuntuacionJugadorFinal({required this.jugador, required this.total});
  
}