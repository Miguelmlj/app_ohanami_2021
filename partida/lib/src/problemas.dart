class ProblemaNombreJugadorVacio extends Error{}
class ProblemaJugadoresRepetidos extends Error{}
class ProblemaNumeroJugadoresMenorMinimo extends Error{}
class ProblemaNumeroJugadoresMayorMaximo extends Error{}
class ProblemaAzulesNegativas extends Error{}
class ProblemasDemasiadasAzules extends Error{}
class ProblemaJugadoresNoConcuerdan extends Error{}
class ProblemaVerdesNegativas extends Error{}
class ProblemaDemasiadasAzules extends Error{}
class ProblemaOrdenIncorrecto extends Error{}
class ProblemaDisminucionAzules extends Error{}
class ProblemaDemasiadasVerdes extends Error{}
class ProblemaExcesoCartas extends Error{}

//ProblemaVerdesNegativas ProblemaDemasiadasAzules

class ProblemaNegrasNegativas extends Error{} 
class ProblemaRosasNegativas extends Error{}
class ProblemaDisminucionVerdes extends Error{}
class ProblemaDemasiadasNegras extends Error{}
class ProblemaDemasiadasRosas extends Error{}

class ProblemaListaPuntuacionJugadorVacia extends Error{}